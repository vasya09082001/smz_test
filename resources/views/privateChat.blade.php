@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<private-chat-component></private-chat-component>
			</div>
		</div>
	</div>
@endsection