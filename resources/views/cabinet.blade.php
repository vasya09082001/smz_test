@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#cab">Кабинет</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#stat">Статистика</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#fns">Привязка к ФНС</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div id="cab" class="container tab-pane active">
                        <div class="card-body">
                            <div class="alert" role="alert">
                              Имя: {{ Auth::user()->name }}<br>
                                email: {{ Auth::user()->email }}
                            </div>
                        </div>
                    </div>
                    <div id="stat" class="container tab-pane fade">
                        <div class="card-body">
                            stat
                        </div>
                    </div>
                    <div id="fns" class="container tab-pane fade">
                        <div class="card-body">
                            <button class="btn btn-primary">Привязка</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
