@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-3 col-sm-3 col-users">
				<ul class="list-group users">
					<users-chat-component
						v-for="value,index in users"
					    :user = chat.user[index]
					>
						@{{ value.name }}
					</users-chat-component>
				</ul>
			</div>
			<div class="col-9 col-sm-9">
				<h4 class="text-center alert-info title">
					<span class="badge badge-primary float-left">@{{ numberOfUsers }} онлайн</span>
					Здесь можно задать вопрос консультанту
				</h4>

				<ul class="list-group messages">
{{--					<vueScroll>--}}
						<chat-component
							v-for="value,index in chat.message"
							:key=value.index
							:color= chat.color[index]
							:user = chat.user[index]
							:time = chat.time[index]
						>
							@{{ value }}
						</chat-component>
{{--					</vueScroll>--}}
				</ul>
				<div class="badge badge-pill badge-primary">@{{ typing }}</div>
				<input type="text" class="form-control" placeholder="Введите сообщение..." v-model='message' @keyup.enter='sendMessage'>
				{{--<v-btn depressed small color="purple">Primary</v-btn>
				{{--<upload-file-component></upload-file-component>--}}
			</div>
		</div>
	</div>
@endsection
<style>
	.title{
		padding:10px
	}
	.messages{
		height: 300px;
	}
	.col-users {
		font-size: 15px;
		background-color: #fff;
		width: 20rem;
		position: fixed;
		z-index: 10;
		margin: 0;
		top: 0.6rem;
		left: 0;
		bottom: 0;
		box-sizing: border-box;
		border-right: 1px solid #eaecef;
		overflow-y: auto;
	}
	.users{
		height: 100%;

		width: 100%;

		padding: 0;

		position: relative;

		overflow: hidden;
	}
</style>