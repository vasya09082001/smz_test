{{ Form::model($user, ['url' => route('bindPartnerInn')]) }}
@if ($errors->any())
	<div>
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif

{{ Form::label('inn', 'Ваш ИНН') }}
	{{ Form::text('inn') }}<br>

{{--{{ Form::label('tel', 'Ваш номер телефона') }}--}}
{{--	{{ Form::text('tel') }}<br>--}}

{{ Form::submit('Сохранить') }}
{{ Form::close() }}