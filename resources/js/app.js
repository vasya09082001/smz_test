/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

//import Vue from 'vue';

//for scroll todo
//import VueChatScroll from 'vue-chat-scroll';
//Vue.use(VueChatScroll);

//for notifications
import 'v-toaster/dist/v-toaster.css';
import Toaster from 'v-toaster';
Vue.use(Toaster,{timeout:10000});

// const VueUploadComponent = require('vue-upload-component').default;
//Vue.component('file-upload',  require('vue-upload-component').default);

// import vuescroll from 'vuescroll/dist/vuescroll-slide';
// Vue.use(vuescroll);

import Vuetify from '../plugins/vuetify' // path to vuetify export
import '../sass/app.scss'
window.Vuetify = require('vuetify');
Vue.use(Vuetify);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('chat-component', require('./components/ChatComponent.vue').default);
Vue.component('users-chat-component', require('./components/UsersChatComponent.vue').default);
Vue.component('private-chat-component', require('./components/PrivateChatComponent.vue').default);
Vue.component('upload-file-component', require('./components/UploadFileComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data() {
        return {
            message: '',
            chat:{
                message:[],
                user:[],
                color:[],
                time:[]
            },
            typing:'',
            numberOfUsers:0,
            users:[],
        }
    },
    watch:{
        message(){
            Echo.private('chat')
                .whisper('typing', {
                    name: this.message,
                    user: this.chat.user
                });
        }
    },
    mounted() {
        // axios.post('/getOldMessage')
        //     .then(response => {
        //         console.log(response);
        //         if (response.data !== '') {
        //             this.chat = response.data;
        //         }
        //     })
        //     .catch(error => {
        //         console.log(error);
        //     });
        window.Echo.private('chat')
            .listen('ChatEvent', (e) => {
                console.log(e);
                this.chat.message.push(e.message);
                this.chat.user.push(e.user);
                this.chat.color.push('warning');
                this.chat.time.push(this.getTime());

                axios.post('/saveToSession',{
                    chat : this.chat
                })
                .then(response => {
                })
                .catch(error => {
                    console.log(error);
                });
            })
            .listenForWhisper('typing', (e) => {

                if(e.name !== '')
                    this.typing ='Собеседник набирает сообщение...'; // e.user[0] +
                else
                    this.typing = ''; 
            });
        window.Echo.join(`chat`)
            .here((users) => {
                console.log(users);
                this.users = users;
                this.numberOfUsers = users.length;
            })
            .joining((user) => {
                this.numberOfUsers +=1;
                this.$toaster.success(user.name + ' присоединелся к чату');
                this.users.push(user);
                // console.log(user.name);
            })
            .leaving((user) => {
                this.numberOfUsers -=1;
                this.$toaster.warning(user.name + ' покинул чат');

                var index = this.users.indexOf(user);
                if (index > -1) {
                    this.users.splice(index, 1);
                }

            });
    },
    methods:{
        sendMessage(){
            if(this.message.length !== 0){
                this.chat.message.push(this.message);
                this.chat.color.push('success');
                this.chat.user.push('Вы');
                this.chat.time.push(this.getTime());

                axios.post('/send', {
                    message: this.message,
                    chat: this.chat
                })
                .then(response => {
                    // console.log(response);
                    this.message = "";
                })
                .catch(error => {
                    console.log(error);
                });
            }
        },
        getTime(){
            let time = new Date();
            return time.getUTCDay()+"."+time.getUTCMonth()+"."+time.getFullYear()+" "+time.getHours()+":"+time.getMinutes();
        },
        getOldMessages(){
            axios.post('/getOldMessage')
                .then(response => {
                    console.log(response);
                    if (response.data !== '') {
                        this.chat = response.data;
                    }
                })
                .catch(error => {
                    console.log(error);
                });
        }
    }
});
