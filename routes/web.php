<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Artisan::call('view:clear'); // dev
//Artisan::call('config:cache'); // dev
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//news
//Route::resource('/articles', 'ArticleController');

//custom
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/new', 'HomeController@new')->name('home');

//chat
Route::get('/chat', 'NewChatController@chat')->name('chat');
Route::post('/send', 'NewChatController@send');
Route::get('/check',function(){
	return session('chat');
});//test
Route::post('saveToSession','NewChatController@saveToSession');
//Route::post('deleteSession','NewChatController@deleteSession');
Route::post('getOldMessage','NewChatController@getOldMessage');


Route::get('/new_chat', 'ChatController@index');
Route::get('/private_chat', 'ChatController@indexNew');
Route::post('/messages', 'ChatController@sendMessage');
Route::post('/private_messages', 'ChatController@sendPrivateMessage');
