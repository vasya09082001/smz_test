<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	protected $fillable = ['name', 'body'];
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
