<?php

namespace App\Http\Controllers\Api;

use App\Notifications\MailMessageSend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Middleware\Fns;

class UserController extends Controller
{
	public $successStatus = 200;
	/**
	 * login api
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function login(){
		if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
			$user = Auth::user();
			$success['token'] =  $user->createToken('MyApp')-> accessToken;
			return response()->json(['success' => $success], $this->successStatus);
		}
		else{
			return response()->json(['error'=>'Unauthorised'], 401);
		}
	}
	/**
	 * Register api
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function register(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'email' => 'required|email',
			'password' => 'required',
			'c_password' => 'required|same:password',
		]);
		if ($validator->fails()) {
			return response()->json(['error'=>$validator->errors()], 401);
		}
		$input = $request->all();
		$input['password'] = bcrypt($input['password']);
		$user = User::create($input);
		$success['token'] =  $user->createToken('MyApp')-> accessToken;
		$success['name'] =  $user->name;
		$user->notify(new MailMessageSend());
		return response()->json(['success'=>$success], $this-> successStatus);
	}
	/**
	 * details api
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function details()
	{
		$user = Auth::user();
		return response()->json(['success' => $user], $this-> successStatus);
	}

	public function bindPartnerInn(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'inn' => 'required|min:10|integer',
			'_token' => 'required'
		]);
		if ($validator->fails())
			return response()->json(['error'=>$validator->errors(), $request->all()], 401);

		$fns = new Fns();
		$access = $fns->PostBindPartnerWithInnRequest($request);
		if($access)
			return response()->json($request,200);
		else
			return response()->json('Ошибка в запросе!',401);
	}

}
