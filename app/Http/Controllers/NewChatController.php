<?php

namespace App\Http\Controllers;

use App\Events\ChatEvent;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class NewChatController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
    public function chat()
	{
		return view('chat');
	}

	/**
	 * @param Request $request
	 */
	public function send(Request $request)
	{
		$user = User::find(Auth::id());
		$this->saveToSession($request);
		event(new ChatEvent($request->message, $user));
	}

	public function saveToSession(Request $request)
	{
		session()->put('chat', $request->message);
	}

	public function getOldMessage()
	{
		return session('chat');
	}

}
