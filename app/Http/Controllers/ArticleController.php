<?php

namespace App\Http\Controllers\Api;

use App\Article;
use App\Http\Requests\BlogPost;
use App\Http\Controllers\Controller;
use Validator;

class ArticleController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$articles =  Article::all();
		return response()->json($articles,200);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(BlogPost $request)
	{
//		$rules = $this->validate($request, [
//			'name' => 'required|unique:articles,name',
//			'body' => 'required|min:100',
//		]);
//		$article = new Article();
//		$article->fill($data);
//		$article->save();
//		return 	response()->json($article,201);
//		if(!$rules)
//			return response()->json($rules->errors(), 400);
//		else{
//			$article = new Article();
//			$article->fill($rules);
//			$article->save();
//		}

//		$article = Article::create($request->all());
		$article = Article::create($request->validated());
		return response()->json($article, 201);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Article  $article
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$article = Article::findOrFail($id);
		return response()->json($article,200);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Article  $article
	 * @return \Illuminate\Http\Response
	 */
	public function update(BlogPost $request, $id)
	{
		$article = Article::findOrFail($id);
		$data = $this->validate($request, [
			'name' => 'required|unique:articles,name,' . $article->id,
			'body' => 'required|min:100',
		]);
		$article->fill($data);
		$article->save();
		return response()->json($article, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Article  $article
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$article = Article::find($id);
		if ($article)
			$article->delete();
		else
			return response()->json(["message" => "Record not found!"], 404);
		return response()->json(null, 204);
	}
}
