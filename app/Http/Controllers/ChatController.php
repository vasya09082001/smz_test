<?php

namespace App\Http\Controllers;

use App\Events\Message;
use App\Events\PrivateMessage;
use Illuminate\Http\Request;

class ChatController extends Controller
{
	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		return view('chat');
	}
	public function indexNew()
	{
		return view('new-chat');
	}

	/**
	 * @param Request $request
	 */
    public function sendMessage(Request $request)
	{
		Message::dispatch($request->input('body'));
	}
	public function sendPrivateMessage(Request $request)
	{
		PrivateMessage::dispatch($request->all());
	}
}
