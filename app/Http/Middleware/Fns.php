<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request as RequestGuzzle;
use GuzzleHttp\Psr7\Response;

class Fns
{
	public $token;

	public function __construct()
	{
		$this->token = $this->AccessToken();
	}

	/**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    private function AccessToken()
	{
		$client = new Client();
		$URI = 'https://ktir.smz.w1.money/api/token/';
		$params['headers'] = ['Content-Type' => 'application/x-www-form-urlencoded'];
		$params['form_params'] = [
				'username' => config("app.username"),
				'password' => config("app.password"),
				'grant_type' => 'password',
				'client_id' => 'smz-api'
			];
		try{
			$response = $client->post($URI, $params);
			if($response->getStatusCode() == 200){
				$body = $response->getBody();
				$token = json_decode($body);
				echo $token->access_token;
				return $token->access_token;
			}
			else{
				echo 'Unexpected HTTP status: ' . $response->getStatusCode() . ' ' . $response->getReasonPhrase();
			}

		}
		catch (RequestException $e){
			echo 'Error: ' . $e->getMessage();
		}

	}

	public function PostBindPartnerWithInnRequest(Request $request)
	{
		$client = new Client();
		$URI = 'https://ktir.smz.w1.money/api/smz/postBindPartnerWithInnRequest?externalId='.config('app.EXTERNAL_ID');
		$params['headers'] = ['Content-Type' => 'application/json','Accept: application/json', 'Authorization' => "Bearer {$this->token}"];
		$params['form_params'] = [
			'inn' => $request->inn,
			'permissions' => [
				'PAYMENT_INFORMATION',
				'TAX_PAYMENT',
				'INCOME_REGISTRATION',
				'INCOME_LIST',
				'INCOME_SUMMARY'
			]
		];
		try{
			$response = $client->post($URI, $params);
			if($response->getStatusCode() == 200){
				$body = $response->getBody();
				echo $body;
			}
			else{
				echo 'Unexpected HTTP status: ' . $response->getStatusCode() . ' ' . $response->getReasonPhrase();
			}

		}
		catch (RequestException $e){
			echo 'Error: ' . $e->getMessage();
		}
		return true;
	}
}
