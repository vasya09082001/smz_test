<?php

namespace App\Http\Middleware;

use Closure;
use Phpml\Classification\KNearestNeighbors;
use Phpml\Regression\LeastSquares;

class Math
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

	public function index()
	{
		$samples = [1, 2, 3, 4, 5];//month
		$targets = [60, 50, 41, 36, 24];//income

		$regression = new KNearestNeighbors();
		$regression->train($samples, $targets);
		//$result = $regression->predict([6]);
		//$x = MathAlgoritmController::incomeMath(6);
		$x = Math::linear_regression($samples, $targets);

//		return view('welcome', compact( 'x'));
	}

	public function incomeMath($x)
	{
		$regression = new LeastSquares();
		$intercept = $regression->getIntercept();
		// return -7.9635135135131
		$coef = $regression->getCoefficients();
		$regression->getCoefficients();
		// return [array(1) {[0]=>float(0.18783783783783)}]

		$y = (float)$intercept + (float)$coef * (float)$x;
		return $y;
	}

	public function linear_regression( $x, $y )
	{
		$n     = count($x);     // number of items in the array
		$x_sum = array_sum($x); // sum of all X values
		$y_sum = array_sum($y); // sum of all Y values

		$xx_sum = 0;
		$xy_sum = 0;

		for($i = 0; $i < $n; $i++) {
			$xy_sum = $xy_sum + ( $x[$i]*$y[$i] );
			$xx_sum = $xx_sum+  ( $x[$i]*$x[$i] );
		}

		// Slope
		$slope = ( ( $n * $xy_sum ) - ( $x_sum * $y_sum ) ) / ( ( $n * $xx_sum ) - ( $x_sum * $x_sum ) );

		// calculate intercept
		$intercept = ( $y_sum - ( $slope * $x_sum ) ) / $n;

		$y = $intercept + $slope * 6;//x
		return $y;
	}
}
